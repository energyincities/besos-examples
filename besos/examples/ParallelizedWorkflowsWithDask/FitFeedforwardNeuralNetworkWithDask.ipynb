{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fit feedforward Neural Network model With Dask\n",
    "This notebook takes the \"Fit feedforward Neural Network model\" notebook and parallelizes the processes using Dask.\n",
    "It will skip over explanation of code unrelated to Dask. Refer to the \"Fit feedforward Neural Network model\" notebook for more details on this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First import packages, and initialize the scheduler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import joblib\n",
    "from besos import eppy_funcs as ef, sampling\n",
    "from besos.evaluator import EvaluatorEP, EvaluatorGeneric\n",
    "from besos.problem import EPProblem\n",
    "from dask.distributed import Client\n",
    "from sklearn.model_selection import GridSearchCV, train_test_split\n",
    "from sklearn.neural_network import MLPRegressor\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "import warnings\n",
    "from parameter_sets import parameter_set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dask.distributed import Client\n",
    "client = Client()\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We gather the parameters and the building, then create the problem and evaluator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     0,
     1,
     59,
     60
    ]
   },
   "outputs": [],
   "source": [
    "parameters = parameter_set(7)\n",
    "problem = EPProblem(parameters, [\"Electricity:Facility\"])\n",
    "building = ef.get_building()\n",
    "problem = EPProblem(parameters, ['Electricity:Facility'])\n",
    "evaluator = EvaluatorEP(problem, building)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When df_apply is called, the dataframe will be processed concurrently. By passing in the `processes` parameter you can define the number of paritions the dataframe will be divided into.\n",
    "If you are running this notebook locally, you can open the Dask dashboard. A link is provided by the `client` object (refer to the first cell in the notebook where we initialized `Client`). On the dashboard, you can see what processes are running."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "%%time\n",
    "inputs = sampling.dist_sampler(sampling.lhs, problem, 50)\n",
    "outputs = evaluator.df_apply(inputs, processes=4)\n",
    "inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up model parameters\n",
    "In this cell, we setup the model. More detail can be found in the \"Fit feedforward Neural Network model\"  notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_in, test_in, train_out, test_out = train_test_split(\n",
    "    inputs, outputs, test_size=0.2\n",
    ")\n",
    "\n",
    "scaler = StandardScaler()\n",
    "inputs = scaler.fit_transform(X=train_in)\n",
    "\n",
    "scaler_out = StandardScaler()\n",
    "outputs = scaler_out.fit_transform(X=train_out)\n",
    "\n",
    "hyperparameters = {\n",
    "    \"hidden_layer_sizes\": (\n",
    "        (len(parameters) * 16,),\n",
    "        (len(parameters) * 16, len(parameters) * 16),\n",
    "    ),\n",
    "    \"alpha\": [1, 10, 10 ** 3],\n",
    "}\n",
    "\n",
    "neural_net = MLPRegressor(max_iter=1000, early_stopping=False)\n",
    "folds = 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model fitting with Dask"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we use the NN model from ScikitLearn.\n",
    "In a [different example](FitNNTF.ipynb) we use TensorFlow (with and without the Keras wrapper).\n",
    "\n",
    "Below we parallelize the model fit.\n",
    "Normally, SciketLearn uses joblib to parallelize model fitting. By specifying the parrallel backend to be Dask, joblib switches over to using the Dask scheduler.\n",
    "For this example, using Dask may not be any faster. This is because joblib also has the ability to parrallelize accross cores.\n",
    "An example where this tool would be useful is when Dask is using a ditributed network with access to more cores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "with joblib.parallel_backend(\"dask\"):\n",
    "    clf = GridSearchCV(neural_net, hyperparameters, iid=True, cv=folds)\n",
    "    with warnings.catch_warnings():\n",
    "        warnings.simplefilter(\"ignore\", category=FutureWarning)\n",
    "        clf.fit(inputs, outputs.ravel())\n",
    "\n",
    "print(f\"Best performing model $R^2$ score on training set: {clf.best_score_}\")\n",
    "print(f\"Model $R^2$ parameters: {clf.best_params_}\")\n",
    "print(\n",
    "    f\"Best performing model $R^2$ score on a separate test set: {clf.best_estimator_.score(scaler.transform(test_in), scaler_out.transform(test_out))}\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Surrogate Modelling Evaluator object\n",
    "We can wrap the fitted model in a BESOS `Evaluator`.\n",
    "This has identical behaviour to the original EnergyPlus Evaluator object.\n",
    "\n",
    "The parrallelization occurs when calling the df_apply function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def evaluation_func(ind, scaler=scaler):\n",
    "    ind = scaler.transform(X=[ind])\n",
    "    return (scaler_out.inverse_transform(clf.predict(ind))[0],)\n",
    "\n",
    "\n",
    "NN_SM = EvaluatorGeneric(evaluation_func, problem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running a large surrogate evaluation\n",
    "Here we bump up the sample count to 50,000 and partition the data into 4. (if you have more cores available, feel free to try increasing the proccesses)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "inputs = sampling.dist_sampler(sampling.lhs, problem, 50000)\n",
    "outputs = NN_SM.df_apply(inputs, processes=4)\n",
    "results = inputs.join(outputs)\n",
    "results.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
